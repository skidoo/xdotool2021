xdotool enables you simulate keyboard input and mouse activity, move and resize windows, etc. It does this using X11’s XTEST extension and other Xlib functions.

Additionally, you can search for windows and move, resize, hide, and modify window properties like the title. If your window manager supports it, you can use xdotool to switch desktops, move windows between desktops, and change the number of desktops.

. .

git clone https://salsa.debian.org/debian/xdotool

debian/control (and debian/compat): specify 10 as minimum debhelper version

installed autoconf    2.69-10
installed automake    1:1.15-6
installed autopoint    0.19.8.1-2+deb9u1
installed autotools-dev    20161112.1
installed build-essential:amd64     12.3
installed debhelper    10.2.5
installed desktop-file-utils:amd64     0.23-1
installed dh-autoreconf    14
installed dh-exec:amd64     0.23+b1
installed dh-strip-nondeterminism    0.034-1
installed doxygen:amd64     1.8.13-4+b1
installed fakeroot:amd64     1.21-3.1
installed g++-6:amd64     6.3.0-18+deb9u1
installed g++:amd64     4:6.3.0-4
installed git:amd64     1:2.11.0-3+deb9u7
installed git-man    1:2.11.0-3+deb9u7
installed hicolor-icon-theme    0.15-1
installed libarchive-zip-perl    1.59-1+deb9u1
installed libc-bin:amd64     2.24-11+deb9u4
installed libclang1-3.9:amd64     1:3.9.1-9
installed liberror-perl    0.17024-1
installed libfakeroot:amd64     1.21-3.1
installed libfile-stripnondeterminism-perl    0.034-1
installed libobrender32v5:amd64     3.6.1-4
installed libobt2v5:amd64     3.6.1-4
installed libperl5.24:amd64     5.24.1-3+deb9u7
installed libpthread-stubs0-dev:amd64     0.3-4
installed libruby2.3:amd64     2.3.3-1+deb9u9
installed libstdc++-6-dev:amd64     6.3.0-18+deb9u1
installed libtimedate-perl    2.3000-2+deb9u1
installed libtool    2.4.6-2
installed libx11-6:amd64     2:1.6.4-3+deb9u3
installed libx11-dev:amd64     2:1.6.4-3+deb9u3
installed libxau-dev:amd64     1:1.0.8-1
installed libxcb1-dev:amd64     1.12-3
installed libxdmcp-dev:amd64     1:1.1.2-3
installed libxext-dev:amd64     2:1.3.3-1+b2
installed libxfixes-dev:amd64     1:5.0.3-1
installed libxi-dev:amd64     2:1.7.9-1
installed libxinerama-dev:amd64     2:1.1.3-1+b3
installed libxkbcommon-dev:amd64     0.7.1-2~deb9u1
installed libxtst-dev:amd64     2:1.2.3-1
installed libyaml-0-2:amd64     0.1.7-2
installed m4:amd64     1.4.18-1
installed man-db:amd64     2.7.6.1-2
installed menu:amd64     2.1.47+b1
installed mime-support    3.60
installed openbox:amd64     3.6.1-4
installed perl:amd64     5.24.1-3+deb9u7
installed perl-base:amd64     5.24.1-3+deb9u7
installed perl-modules-5.24    5.24.1-3+deb9u7
installed pkg-config:amd64     0.29-4+b1
installed rake    10.5.0-2+deb9u1
installed ruby2.3:amd64     2.3.3-1+deb9u9
installed ruby:amd64     1:2.3.3
installed ruby-did-you-mean    1.0.0-2
installed rubygems-integration    1.11
installed ruby-minitest    5.9.0-1
installed ruby-net-telnet    0.1.1-2
installed ruby-power-assert    0.3.0-1
installed ruby-test-unit    3.1.7-2
installed sgml-base    1.29
installed x11proto-core-dev    7.0.31-1
installed x11proto-fixes-dev    1:5.0-2
installed x11proto-input-dev    2.3.2-1
installed x11proto-kb-dev    1.0.7-1
installed x11proto-record-dev    1.14.2-1
installed x11proto-xext-dev    7.3.0-1
installed x11proto-xinerama-dev    1.2.1-2
installed xbitmaps    1.1.1-2
installed xorg-sgml-doctools    1:1.11-1
installed xterm:amd64     327-2
installed xtrans-dev    1.3.5-1
installed xvfb:amd64     2:1.19.2-1.0nosystemd5
upgraded  libperl5.24:amd64     5.24.1-3+deb9u5     5.24.1-3+deb9u7
upgraded  libx11-6:amd64     2:1.6.4-3+deb9u1     2:1.6.4-3+deb9u3
upgraded  perl:amd64     5.24.1-3+deb9u5     5.24.1-3+deb9u7
upgraded  perl-base:amd64     5.24.1-3+deb9u5     5.24.1-3+deb9u7
upgraded  perl-modules-5.24    5.24.1-3+deb9u5     5.24.1-3+deb9u7


noted during a successful test build: the included patches are UNapplied

https://salsa.debian.org/debian/xdotool/-/commits/debian/master
^------- most recent commit (the codebase I grabbed) was 2018

xref: https://sources.debian.org/src/xdotool/

https://packages.debian.org/sid/xdotool
^---- debian pkg maintainer recognizes the project homepage as  https://github.com/jordansissel/xdotool

https://bugs.debian.org/cgi-bin/pkgreport.cgi?pkg=xdotool;dist=unstable
^----- currently 6 open (normal..wishlist) bugtickets from 2014--2017


   v-------- retrieved this (from 2010) for inspection
https://sourceforge.net/projects/xdotool-gui/files/xdotoolgui_1.2-1.tar.gz/download
(added to this project, inside an "extra" subdirectory


https://www.semicomplete.com/projects/xdotool/
" The xdotool users mailing list is: xdotool-users@googlegroups.com "

Noted (Oct 5, 2020)
xdotool currently has  174  open issue tickets  https://github.com/jordansissel/xdotool/issues
and  28  open merge requests  https://github.com/jordansissel/xdotool/pulls
e.g. https://github.com/jordansissel/xdotool/pull/213


noted, for possible followup:
https://github.com/xvol/bezmouse (point-to-point via semi-randomized bezier curves)
https://github.com/eHonnef/afk-clicker  ( simulate keystrokes ~~ mouse or keyboard ~~ even if the window isn't focused)
https://github.com/corywalker/x11_automate (python xdotool wrapper)
https://github.com/AndrewTerekhine/xkeys (desktop automation for Linux and X11 written in Python and based on xclip, xsel, wmctr, xdotool, xbindkeys)
https://github.com/moverest/xdotool-key-convert  a simple tool to convert a string to a sequence of keys to be passed to xdotool
https://github.com/valenca/gauche/blob/master/funcs/travel.py
https://github.com/andipanic/autoscroll   (wannabe tachistoscope, mousewheel)
https://github.com/betafcc/xpectacle/tree/master/xpectacle
.
https://github.com/dj311/uniclick
uniclick takes a screenshot of your desktop, runs optical character recognition (OCR) on it,
then displays narrowing UI that allows you to select a word. Once your target word is selected,
you cursor will be moved to its center. uniclick will click or double-click on request. It's similar in spirit to keynav.
.
https://github.com/okitavera/cornora
