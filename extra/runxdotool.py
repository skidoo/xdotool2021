#!/usr/bin/env python2
#
# License: GPLv2, or later

import gtk, subprocess, time, string
def AddCommandToRun(CommandPath):
    print CommandPath
    afile = open(CommandPath)
    ParamsForCommand = afile.read().split("\n")
    for s in ParamsForCommand:
        EachCommand = s.split(" ")
        if EachCommand[0] == 'move':
            subprocess.check_call(["xdotool","mousemove",EachCommand[3],EachCommand[4]])
        if EachCommand[0] == 'delay':
            time.sleep(string.atof(EachCommand[1]))
        if EachCommand[0] == 'click':
            if EachCommand[2] == 'left':
                for i in EachCommand[5]:
                    subprocess.check_call(["xdotool","click","1"])
                    time.sleep(string.atof(EachCommand[9]))
            if EachCommand[2] == 'middle':
                for i in EachCommand[5]:
                    subprocess.check_call(["xdotool","click","2"])
                    time.sleep(string.atof(EachCommand[9]))
            if EachCommand[2] == 'right':
                for i in EachCommand[5]:
                    subprocess.check_call(["xdotool","click","3"])
                    time.sleep(string.atof(EachCommand[9]))
        if EachCommand[0] == 'type':
            for i in EachCommand[5]:
                subprocess.check_call(["xdotool","key",EachCommand[3]])
                time.sleep(string.atof(EachCommand[9]))
    return 0


def main():
    return 0


if __name__ == '__main__': main()
